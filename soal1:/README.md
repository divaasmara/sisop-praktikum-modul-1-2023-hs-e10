nama file csv saya rename menjadi x.csv untuk mempermudah pengerjaan;

1a. grep lines yang memiliki kata "Japan" pada file x.csv kemudian hasilnya diambil 5 teratas menggunakan head -n 5 <br />
1b. hasil dari 1a. di sort berdasarkan kolom ke 9 (fsr) dalam ascending order, -n untuk sort dalam numerical stardard, -t ',' untuk
    mengisyaratkan bahwa data dipisahkan oleh tanda ',' <br />
1c. grep lines yang memiliki kata "Japan" pada file x.csv kemudian hasilnya di sort berdasarkan kolom ke 20 (ger) dalam ascending order,
    -n untuk sort dalam numerical stardard, -t ',' untuk mengisyaratkan bahwa data dipisahkan oleh tanda ',' kemudian diambil 10 teratas
    dengan head -n 10 <br />
1d. grep lines yang memiliki kata "keren" pada file x.csv <br />
